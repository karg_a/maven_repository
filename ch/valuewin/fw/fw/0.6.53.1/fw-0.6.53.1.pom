<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<!-- POM file generated with GWT webAppCreator -->
	<modelVersion>4.0.0</modelVersion>
	<groupId>ch.valuewin.fw</groupId>
	<artifactId>fw</artifactId>
	<packaging>jar</packaging>
	<version>0.6.53.1</version>
	<name>framework</name>


	<properties>
		<gwt.version>2.8.1</gwt.version>
		<gwtBootstrap3.version>1.0.patch.1</gwtBootstrap3.version>
		<camunda.version>7.1.0-Final</camunda.version>
		<bouncycastle.version>1.46</bouncycastle.version>
		<spring.version>4.3.10.RELEASE</spring.version>
		<spring.security.version>3.2.10.RELEASE</spring.security.version>
		<querydsl.version>3.7.4</querydsl.version>
		<jackson.version>2.8.9</jackson.version>
		<slf4j.version>1.7.25</slf4j.version>
		<java.version>1.8</java.version>
		<javasimon.version>4.2.0</javasimon.version>
		<webappDirectory>${project.build.directory}/${project.build.finalName}</webappDirectory>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.tomcat.update>true</maven.tomcat.update>
		<asm.version>5.2</asm.version>
		<activemq.version>5.15.0</activemq.version>
	</properties>



	<dependencies>


		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.6</version>
			<scope>provided</scope>
		</dependency>
		
		<!-- need to include this only so that JSON serializer works, see AsynchronousOperationAbstractResult -->
		<dependency>
			<groupId>ch.valuewin.fw</groupId>
			<artifactId>rest-api</artifactId>
			<version>4.0</version>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<!-- version>4.3.5</version -->
			<!-- version>4.5.2</version -->
			<version>4.5.10</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>4.4.12</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<!-- dependency> <version>1.0</version> <scope>system</scope> <systemPath>${basedir}/classpath</systemPath> 
			</dependency -->

		<!-- https://mvnrepository.com/artifact/xml-apis/xml-apis -->
		<dependency>
			<groupId>xml-apis</groupId>
			<artifactId>xml-apis</artifactId>
			<version>1.4.01</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>nl.garvelink.oss</groupId>
			<artifactId>iban</artifactId>
			<version>1.5.0</version>
		</dependency>


		<dependency>
			<groupId>com.thoughtworks.xstream</groupId>
			<artifactId>xstream</artifactId>
			<version>1.4.9</version>
		</dependency>

		<dependency>
			<groupId>com.googlecode.gwtquery</groupId>
			<artifactId>gwtquery</artifactId>
			<version>1.5-beta1</version>
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>org.ocpsoft.prettytime</groupId>
			<artifactId>prettytime</artifactId>
			<version>4.0.1.Final</version>
		</dependency>

		<dependency>
			<groupId>com.mchange</groupId>
			<artifactId>c3p0</artifactId>
			<version>0.9.5.2</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.piwik</groupId>
			<artifactId>gwt-piwik-wrapper</artifactId>
			<version>1.1</version>
			<scope>compile</scope>
		</dependency>


		<dependency>
			<groupId>com.stripe</groupId>
			<artifactId>stripe-java</artifactId>
			<version>1.38.0</version>
		</dependency>

		<dependency>
			<groupId>com.arcbees.stripe</groupId>
			<artifactId>gwt-stripe</artifactId>
			<version>0.2.0</version>
		</dependency>


		<!-- dependency>
			<groupId>org.camunda.bpm.webapp</groupId>
			<artifactId>camunda-webapp</artifactId>
			<version>7.2.0-alpha2</version>
			<classifier>classes</classifier>
			<scope>provided</scope>
		</dependency-->
		
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-jaxrs</artifactId>
			<version>2.3.5.Final</version>
		</dependency>

		<!-- dependency>
			<groupId>org.camunda.bpm.webapp</groupId>
			<artifactId>camunda-webapp</artifactId>
			<version>7.2.0-alpha2</version>
			<type>war</type>
		</dependency-->

		<dependency>
			<groupId>org.camunda.bpm.extension</groupId>
			<artifactId>camunda-bpm-assert</artifactId>
			<version>1.1</version>
		</dependency>


		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
			<version>2.3.23</version>
		</dependency>


		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-s3</artifactId>
			<version>1.11.192</version>
			<scope>provided</scope>
		</dependency>

		<!-- needed by spring servlet controller to support file upload -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.3</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>2.8.1</version>
		</dependency>

		<dependency>
			<groupId>com.mattbertolini</groupId>
			<artifactId>hermes</artifactId>
			<version>1.2.0</version>
			<scope>provided</scope>
		</dependency>


		<!-- Core JAR is absolutely necessary to use Simons (Stopwatch, etc.) -->
		<dependency>
			<groupId>org.javasimon</groupId>
			<artifactId>javasimon-core</artifactId>
			<version>${javasimon.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- This one allows monitoring JDBC calls (proxy driver) -->
		<dependency>
			<groupId>org.javasimon</groupId>
			<artifactId>javasimon-jdbc41</artifactId>
			<version>${javasimon.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- JavaEE support, servlet filter, EJB/CDI interceptor -->
		<dependency>
			<groupId>org.javasimon</groupId>
			<artifactId>javasimon-javaee</artifactId>
			<version>${javasimon.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- Spring support, AOP interceptor, MVC handler interceptor -->
		<dependency>
			<groupId>org.javasimon</groupId>
			<artifactId>javasimon-spring</artifactId>
			<version>${javasimon.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- Embedded Java Simon web console -->
		<dependency>
			<groupId>org.javasimon</groupId>
			<artifactId>javasimon-console-embed</artifactId>
			<version>${javasimon.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jdt.core.compiler</groupId>
			<artifactId>ecj</artifactId>
			<version>4.6.1</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcprov-jdk16</artifactId>
			<version>${bouncycastle.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcmail-jdk16</artifactId>
			<version>${bouncycastle.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcpg-jdk16</artifactId>
			<version>${bouncycastle.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>com.google.api</groupId>
			<artifactId>gwt-oauth2</artifactId>
			<version>0.2-alpha</version>
		</dependency>

		<dependency>
			<groupId>com.postmark</groupId>
			<artifactId>postmark-client</artifactId>
			<version>0.3.2</version>
		</dependency>

		<dependency>
			<groupId>com.googlecode.libphonenumber</groupId>
			<artifactId>libphonenumber</artifactId>
			<version>8.8.1</version>
		</dependency>


		<dependency>
			<groupId>org.geonames</groupId>
			<artifactId>geonames-ws-client</artifactId>
			<version>1.1.9</version>
		</dependency>

		<dependency>
			<groupId>com.google.api-client</groupId>
			<artifactId>google-api-client</artifactId>
			<version>1.22.0</version>
		</dependency>

		<dependency>
			<groupId>com.google.apis</groupId>
			<artifactId>google-api-services-oauth2</artifactId>
			<version>v2-rev129-1.22.0</version>
		</dependency>

		<dependency>
			<groupId>com.google.apis</groupId>
			<artifactId>google-api-services-calendar</artifactId>
			<version>v3-rev254-1.22.0</version>
		</dependency>

		<dependency>
			<groupId>com.google.gdata</groupId>
			<artifactId>core</artifactId>
			<version>1.47.1</version>
			<exclusions>
				<exclusion>
					<groupId>com.google.oauth-client</groupId>
					<artifactId>google-oauth-client-jetty</artifactId>
				</exclusion>
			</exclusions>
		</dependency>


		<dependency>
			<groupId>org.ow2.asm</groupId>
			<artifactId>asm</artifactId>
			<version>${asm.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.ow2.asm</groupId>
			<artifactId>asm-commons</artifactId>
			<version>${asm.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<!-- needed for querydsl -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>18.0</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>org.springframework.security.oauth</groupId>
			<artifactId>spring-security-oauth2</artifactId>
			<version>2.2.0.RELEASE</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>javax.annotation</groupId>
			<artifactId>jsr250-api</artifactId>
			<version>1.0</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>org.gwtbootstrap3</groupId>
			<artifactId>gwtbootstrap3</artifactId>
			<version>${gwtBootstrap3.version}</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.gwtbootstrap3</groupId>
			<artifactId>gwtbootstrap3-extras</artifactId>
			<version>${gwtBootstrap3.version}</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-user</artifactId>
			<version>${gwt.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-servlet</artifactId>
			<version>${gwt.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jms</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${spring.security.version}</version>
			<!-- This library is provided as shared library to tomcat, but cannot 
				put 'provided' here, else it fails -->
			<!-- scope>provided</scope -->
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
			<version>${spring.security.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${spring.security.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-expression</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

          <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-beans</artifactId>
               <version>${spring.version}</version>
               <!-- provided as shared library to tomcat -->
               <scope>provided</scope>
          </dependency>

          <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-orm</artifactId>
               <version>${spring.version}</version>
               <!-- provided as shared library to tomcat -->
               <scope>provided</scope>
          </dependency>

          <dependency>
               <groupId>org.springframework</groupId>
               <artifactId>spring-jdbc</artifactId>
               <version>${spring.version}</version>
               <!-- provided as shared library to tomcat -->
               <scope>provided</scope>
          </dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>aopalliance</groupId>
			<artifactId>aopalliance</artifactId>
			<version>1.0</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
			<version>${spring.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
			<version>1.8.1.RELEASE</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>com.google.code.gwtsecurity</groupId>
			<artifactId>gwtsecurity-core</artifactId>
			<version>1.3.3</version>
			<!-- <scope>compile</scope> -->
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.eclipse.persistence</groupId>
			<artifactId>eclipselink</artifactId>
			<version>2.7.6</version>
			<!-- provided as shared library to tomcat -->
			<!-- scope>provided</scope-->
		</dependency>

		<dependency>
			<groupId>org.camunda.bpm</groupId>
			<artifactId>camunda-engine</artifactId>
			<version>${camunda.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.camunda.bpm</groupId>
			<artifactId>camunda-engine-spring</artifactId>
			<version>${camunda.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis-spring</artifactId>
			<!-- The version 1.2.0 works with MyBatis version 3.1.1, which is default 
				from camunda, newer versions of MyBatis are broken my camunda. And, newer 
				versions of mybatis-spring don't work with older MyBatis -->
			<!-- older versions of mybatis-spring don't support annotation @MapperScan 
				needed for Java Config -->
			<version>1.2.0</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis</artifactId>
			<version>3.1.1</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>42.1.4</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.mysema.querydsl</groupId>
			<artifactId>querydsl-apt</artifactId>
			<version>${querydsl.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.mysema.querydsl</groupId>
			<artifactId>querydsl-jpa</artifactId>
			<version>${querydsl.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-mvp-client</artifactId>
			<version>1.0.3</version>
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>com.googlecode.slotted</groupId>
			<artifactId>slotted</artifactId>
			<version>0.4</version>
			<exclusions>
				<exclusion>
					<groupId>com.google.gwt</groupId>
					<artifactId>gwt-dev</artifactId>
				</exclusion>
			</exclusions>

		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
			<scope>provided</scope>
		</dependency>

		<!-- Jackson JSON Mapper -->
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-mapper-asl</artifactId>
			<version>1.9.10</version>
		</dependency>

		<!-- dependency> <groupId>com.google.gwt</groupId> <artifactId>gwt-dev</artifactId> 
			<version>2.6.0</version> <scope>provided</scope> </dependency -->

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
		</dependency>

		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>2.45.0</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>2.45.0</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jul-to-slf4j</artifactId>
			<version>${slf4j.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.3</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<!-- needed for automatic JSON for web services to work -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>1.0.0.GA</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator-annotation-processor</artifactId>
			<version>4.1.0.Final</version>
			<!-- provided as shared library to tomcat <scope>provided</scope> -->
		</dependency>

		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-collections4</artifactId>
		    <version>4.4</version>
		</dependency>

		<dependency>
			<groupId>net.sf.ehcache</groupId>
			<artifactId>ehcache</artifactId>
			<version>2.10.4</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<!-- used to transport data for the report -->
		<!-- dependency> <groupId>org.hsqldb</groupId> <artifactId>hsqldb</artifactId> 
			<version>2.3.2</version> </dependency -->

		<!-- <dependency> <groupId>com.thoughtworks.xstream</groupId> <artifactId>xstream</artifactId> 
			<version>1.4.7</version> </dependency> -->

		<!-- <dependency> <groupId>com.postmark</groupId> <artifactId>postmark-client</artifactId> 
			<version>0.3.2</version> </dependency> -->


		<dependency>
			<groupId>org.apache.geronimo.specs</groupId>
			<artifactId>geronimo-jms_1.1_spec</artifactId>
			<version>1.1</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>



		<dependency>
			<groupId>org.apache.activemq</groupId>
			<artifactId>activemq-client</artifactId>
			<version>${activemq.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.activemq</groupId>
			<artifactId>activemq-broker</artifactId>
			<version>${activemq.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.activemq</groupId>
			<artifactId>activemq-spring</artifactId>
			<version>${activemq.version}</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
		</dependency>


		<!-- dependency> <groupId>org.apache.activemq</groupId> <artifactId>activemq-kahadb-store</artifactId> 
			<version>5.10.0</version> < provided as shared library to tomcat > <scope>provided</scope> 
			</dependency -->

		<dependency>
			<groupId>org.apache.xbean</groupId>
			<artifactId>xbean-spring</artifactId>
			<version>4.5</version>
			<!-- provided as shared library to tomcat -->
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-dev</artifactId>
			<version>${gwt.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>1.7.25</version>
               <scope>provided</scope>
		</dependency>


	</dependencies>



	<repositories>
		<repository>
			<id>limmobi-mvn-repo</id>
			<url>https://bitbucket.org/karg_a/maven_repository/raw/releases/</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
			</snapshots>
		</repository>
		<repository>
			<id>common-libs</id>
			<url>https://raw.github.com/ubegun/MavenRepo/master/</url>
		</repository>
		<!-- repository> <id>pentaho-releases</id> <url>http://repository.pentaho.org/artifactory/repo/</url> 
			</repository -->
		<repository>
			<id>gwt-piwik-wrapper</id>
			<url>https://raw.github.com/ubegun/gwt-piwik-wrapper/mvn-repo/</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
			</snapshots>
		</repository>
	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>central</id>
			<name>Maven Plugin Repository</name>
			<url>https://repo1.maven.org/maven2</url>
			<layout>default</layout>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<releases>
				<updatePolicy>never</updatePolicy>
			</releases>
		</pluginRepository>
	</pluginRepositories>


	<build>
		<resources>
			<!-- this enables the mvn versions:set to update the version in properties files as well (besides the version in this pom.xml) -->
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/*.properties</include>
				</includes>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>false</filtering>
				<excludes>
					<exclude>**/*.properties</exclude>
				</excludes>
			</resource>
			<!-- resource> <directory>src/main/resources/ch/valuewin/fw/webapp/client/version</directory> 
				<filtering>true</filtering> </resource -->
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**/*.java</include>
					<include>**/*.gwt.xml</include>
					<include>**/*.ui.xml</include>
					<include>**/*.properties</include>
					<include>**/*.gif</include>
				</includes>
			</resource>
		</resources>

		<!-- Generate compiled stuff in the folder used for developing mode -x-> 
			<outputDirectory>${webappDirectory}/WEB-INF/classes</outputDirectory -->

		<plugins>

			<!-- plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-de‌​pendency-plugin</artifactId> 
				<version>2.10</version> <executions> <execution> <phase>generate-sources</phase> 
				<goals> <goal>build-classpath</goal> </goals> <configuration> <outputFile>${p‌​roject.basedir}/classpath</outputFile> 
				</configuration> </execution> </executions> </plugin -->

			<!-- GWT Maven Plugin -->
			<plugin>
				<groupId>net.ltgt.gwt.maven</groupId>
				<artifactId>gwt-maven-plugin</artifactId>
				<version>1.0-rc-8</version>
				<extensions>true</extensions>
				<executions>
					<execution>
						<goals>
							<goal>compile</goal>
							<goal>test</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<moduleName>ch.valuewin.fw.webapp.ValuewinchFw</moduleName>
					<compilerArgs>
						<arg>-localWorkers</arg>
						<arg>2</arg>
						<arg>-noincremental</arg>
						<arg>-XdisableUpdateCheck</arg>
						<!-- DEV ACCEL PLACEHOLDER -->
					</compilerArgs>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.2</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>com.mysema.maven</groupId>
				<artifactId>apt-maven-plugin</artifactId>
				<version>1.1.3</version>
				<executions>
					<execution>
						<goals>
							<goal>process</goal>
						</goals>
						<configuration>
							<outputDirectory>target/generated-sources/java</outputDirectory>
							<processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>



</project>
